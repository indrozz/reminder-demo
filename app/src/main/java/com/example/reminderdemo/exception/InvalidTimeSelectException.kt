package com.example.reminderdemo.exception

import java.lang.Exception

class InvalidTimeSelectException(message : String) : Exception(message)