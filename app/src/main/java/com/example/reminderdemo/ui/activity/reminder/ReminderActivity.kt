package com.example.reminderdemo.ui.activity.reminder

import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.reminderdemo.R
import com.example.reminderdemo.adapter.CustomAdapter
import com.example.reminderdemo.adapter.ItemViewHolder
import com.example.reminderdemo.databinding.ActivityReminderBinding
import kotlinx.android.synthetic.main.item_reminder.view.*
import java.util.*
import java.util.concurrent.TimeUnit

class ReminderActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener {

    private lateinit var reminderBinding: ActivityReminderBinding
    private lateinit var reminderViewModel: ReminderViewModel
    private lateinit var reminderAdapter: CustomAdapter<String>
    private var tvItemReminder : AppCompatTextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reminderViewModel = ViewModelProvider(this).get(ReminderViewModel::class.java)
        reminderBinding = DataBindingUtil.setContentView(this, R.layout.activity_reminder)
        reminderBinding.reminderViewModel = reminderViewModel

        init()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        reminderViewModel.selectedTime.value = intArrayOf(hourOfDay, minute)
    }

    private fun init() {

        reminderAdapter = object : CustomAdapter<String>(
            R.layout.item_reminder,
            arrayListOf()
        ) {
            override fun viewBinder(holder: ItemViewHolder, objects: String, position: Int) {
                holder.itemView.tvReminder.text = objects
                holder.itemView.tvReminder.setOnClickListener {
                    tvItemReminder = holder.itemView.tvReminder
                    reminderViewModel.openTimePicker.value = Calendar.getInstance()
                }
            }
        }

        reminderBinding.rvReminder.apply {
            layoutManager = LinearLayoutManager(this@ReminderActivity)
            adapter = reminderAdapter
        }

        reminderBinding.tvAddMore.setOnClickListener {
            tvItemReminder = null
            reminderViewModel.openTimePicker.value = Calendar.getInstance()
        }

        reminderViewModel.openTimePicker.observe(this, Observer {
            openTimePicker(it)
        })

        reminderViewModel.formattedSelectedTime.observe(this, Observer {
            if (this.tvItemReminder == null) {
                reminderViewModel.addMoreReminder.value = it
            } else {
                this.tvItemReminder?.text = it
                this.tvItemReminder = null
            }
            reminderBinding.tvSelectedTime.text = it
        })

        reminderViewModel.reminder.observe(this, Observer {
            reminderViewModel.showMessage.value = "Remind after $it"
            executeReminderWorkManager(it)
        })

        reminderViewModel.showMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        reminderBinding.btnChooseTime.setOnClickListener {
            reminderViewModel.openTimePicker.value = Calendar.getInstance()
        }

        reminderViewModel.addMoreReminder.observe(this, Observer {
            reminderAdapter.addItem(it)
        })
    }

    private fun executeReminderWorkManager(it: Long) {
        val constraints = Constraints.Builder()
            .setRequiresBatteryNotLow(true)
            .build()

        val reminderWorkRequest = OneTimeWorkRequest.Builder(ReminderWorker::class.java)
            .setConstraints(constraints)
            .setInitialDelay(it, TimeUnit.MINUTES)
            .build()

        WorkManager.getInstance()
            .enqueue(reminderWorkRequest)
    }

    private fun openTimePicker(cal: Calendar) {
        TimePickerDialog(
            this,
            this,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            false
        ).show()
    }

}