package com.example.reminderdemo.ui.activity.reminder

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.reminderdemo.exception.InvalidTimeSelectException
import com.example.reminderdemo.util.DateUtils
import java.util.*

class ReminderViewModel(application: Application) : AndroidViewModel(application) {

    val openTimePicker = MutableLiveData<Calendar>()

    val selectedTime = MutableLiveData<IntArray>().apply {
        val cal = Calendar.getInstance()
        value = intArrayOf(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE))
    }

    val formattedSelectedTime : LiveData<String> = Transformations.map<IntArray, String>(selectedTime) {

        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, it[0])
        cal.set(Calendar.MINUTE, it[1])

        DateUtils.getDateOfFormat(
            cal,
            DateUtils.DATE_TIME_ONLY_TIME_FORMAT
        )
    }

    val reminder : LiveData<Long> = Transformations.map<IntArray, Long>(selectedTime) {
        val delayTime = getDelayTime(it[0], it[1])
        delayTime.toLong()
    }

    val showMessage = MutableLiveData<String>()

    val addMoreReminder = MutableLiveData<String>().apply {
        value = "06:00 AM"
    }

    private fun getDelayTime(hh : Int, mm : Int): Int {
        val selectedTimeInMin = hh * 60 + mm
        val currCal = Calendar.getInstance()
        val currTimeInMin = currCal.get(Calendar.HOUR_OF_DAY) * 60 + currCal.get(Calendar.MINUTE)

        return (selectedTimeInMin - currTimeInMin)
    }
}