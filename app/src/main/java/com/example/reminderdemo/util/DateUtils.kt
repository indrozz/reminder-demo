package com.example.reminderdemo.util

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Developer : Indrajeet Gupta
 * Date : 21/05/18.
 */
object DateUtils {

    const val DATE_TIME_DEFAULT_FORMAT = "dd MMM, yyyy, hh:mm aa"
    const val DATE_TIME_FORMAT_1 = "yyyy-MM-dd, hh:mm aa"
    const val DATE_TIME_DEFAULT_24_FORMAT = "dd MMM, yyyy, HH:mm"
    const val DATE_TIME_ONLY_DATE_FORMAT_1 = "dd MMM, yyyy"
    const val DATE_TIME_ONLY_DATE_FORMAT_2 = "dd/MM/yyyy"
    const val DATE_TIME_ONLY_DATE_FORMAT_3 = "yyyy-MM-dd"
    const val DATE_TIME_ONLY_DATE_FORMAT_4 = "yyyyMMdd"
    const val DATE_TIME_ONLY_TIME_FORMAT = "hh:mm aa"
    const val DATE_TIME_ONLY_TIME_24_FORMAT = "HH:mm"
    const val DATE_TIME_UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DATE_TIME_BACKEND_FORMAT = "yyyy-MM-dd HH:mm:ss"
    const val DATE_TIME_DATE_DAY_FORMAT = "EEE, dd/MM/yyyy"

    fun getDateOfFormat(calendar : Calendar, format : String = DATE_TIME_DEFAULT_FORMAT, locale : Locale = Locale.ENGLISH) : String =
            SimpleDateFormat(format, locale).format(calendar.timeInMillis)

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDateTime(format : String, locale : Locale = Locale.ENGLISH) : String = SimpleDateFormat(format, locale).format(Date())

    @SuppressLint("SimpleDateFormat")
    fun convertDateTo(dateTime : String, fromFormat: String, toFormat: String, locale : Locale = Locale.ENGLISH): String {

        val utcFormat = SimpleDateFormat(fromFormat, locale)
        utcFormat.timeZone = TimeZone.getTimeZone("UTC")

        var date: Date
        try {
            date = utcFormat.parse(dateTime)
        } catch (e: Exception) {
            date = Date()
            e.printStackTrace()
        }

        val pstFormat = SimpleDateFormat(toFormat, locale)
        pstFormat.timeZone = TimeZone.getDefault()

        return pstFormat.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun convertToStandardFormat(dateTime : String, dateFormat: String = DATE_TIME_UTC_FORMAT, locale : Locale = Locale.ENGLISH) : String {
        val sdf = SimpleDateFormat(DATE_TIME_DEFAULT_FORMAT)
        return sdf.format(SimpleDateFormat(dateFormat, locale).parse(dateTime))
    }

    fun convertUTCToLocal(dateTime : String, toFormat: String = DATE_TIME_DEFAULT_FORMAT) : String =
            convertDateTo(dateTime, DATE_TIME_UTC_FORMAT, toFormat)

    @SuppressLint("SimpleDateFormat")
    fun getDate(dateTime : String, dateFormat : String = DATE_TIME_DEFAULT_FORMAT, locale : Locale = Locale.ENGLISH) : Date = SimpleDateFormat(dateFormat, locale).parse(dateTime)

    fun getCalendar(dateTime: String, dateFormat : String = DATE_TIME_DEFAULT_FORMAT) : Calendar {
        val cal : Calendar = Calendar.getInstance()
        cal.time = getDate(dateTime, dateFormat)
        return cal
    }

    fun getDateOfFormatFromMillis(timeInMillis: Long, format : String = DATE_TIME_DEFAULT_FORMAT, locale : Locale = Locale.getDefault()) : String =
            SimpleDateFormat(format, locale).format(timeInMillis)

    fun convertUTCTimeInMillisToLocal(timeInMillis: Long) : Long {
        val cal = Calendar.getInstance()
        cal.timeInMillis = timeInMillis

        val localDateTime = convertUTCToLocal(getDateOfFormat(cal))
        return getCalendar(localDateTime).timeInMillis
    }
}